package reader.services.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import reader.services.app.databinding.ActivityStatus200Binding

class Status200 : Activity() {
    lateinit var binding: ActivityStatus200Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatus200Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val btnReturn = binding.btnReturn
        btnReturn.setOnClickListener(){
            val ventana: Intent = Intent(applicationContext, NextButton::class.java);
            startActivity(ventana)
        }
    }
}